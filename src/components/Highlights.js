//Applying bootstrap grid system
import {Row, Col, Card} from "react-bootstrap";

export default function Highlights(){
	return(
		<div className="bghighlight">
		<Row className="mt-3 mb-3">
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                <img src="./img/lenovo.jpg" className="img1" />

		                    <h2>Lenovo</h2>
		                </Card.Title>
		                <Card.Text className="len">
		                    Lenovo Group Limited, often shortened to Lenovo is an American Chinese multinational technology company specializing in designing, manufacturing, .
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                <img src="./img/acer.jpg" className="img2" />
		                    <h2>Acer</h2>
		                </Card.Title>
		                <Card.Text className="acer">
		                    Acer Inc is a Taiwanese multinational hardware and electronics corporation specializing in advanced electronics technology, headquartered in Xizhi, ...
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                <img src="./img/toshi2.png" className="img3" />
		                    <h2>Toshiba</h2>
		                </Card.Title>
		                <Card.Text className="toshi">
		                    Toshiba Corporation commonly known as Toshiba and stylized as TOSHIBA, is a Japanese multinational conglomerate headquartered in Minato, Tokyo, Japan.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
		</div>
	)
}