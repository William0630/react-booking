// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

	// We consume te "user" value from the User context to update the Nav links in our navigation bar.
	const {user} = useContext(UserContext);

	// State hook to store the information stored in the login page.
									//null
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);
	/*
		- The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities.
		- The "to" prop is used in place of the "href" prop for providing the URL for the page.
		- "defaultActiveKey" & "eventKey" is used to highlight the active NavLink component that matches the URL.

	*/
	return(
		<Navbar variant="dark" bg="dark" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/" > 

	         <img
              alt=""
              src="logo192.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            

	        </Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	    		{/*className is use instead of "class", to specify a CSS classes*/}
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
	            {
	            	(user.isAdmin)
	            	?
	            	<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
	            	:
	            	<Nav.Link as={Link} to="/products" eventKey="/products">Product Catalog</Nav.Link>
	            }
	            {/*
	            	Before applying useContext hooks:
						user.email is causing an error when the local storage is cleared because the initial state of user is change to null and we can't perform a dot notation to a non-object datatype.
	            */}
	            {	
	            	(user.id !== null)
	            	?
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            	:
	            		<>
		            		<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            		<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
	            		</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}