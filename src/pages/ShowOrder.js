import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

export default function CheckOut(){
    // from local storage
    const list=localStorage.getItem('react-use-cart');
    console.log(list)

    // to validate the user role.
    const {user} = useContext(UserContext);

    // Order State
    const [productCart, setProductCart] = useState([]);

    const orders = () => {
            fetch(`${process.env.REACT_APP_API_URL}/users/Orders`,{
                headers:{
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

            setProductCart(data.map(cart => {
                            return(
                                <tr key={cart._id}>
                                    <td>{cart.name}</td>
                                    <td>{cart.price}</td>
                                    <td>{cart.quantity}</td>
                                    <td>{cart.purchasedOn}</td>
                                </tr>
                            )
                    }))
                })
            }

            useEffect(()=>{
                    // Get orders in the first render
                    orders();
                }, [])

        return(
        <>
            <div className="mt-5 mb-3 text-center">
                    <h1 className="mb-6 text-dark">ORDERS</h1>
                </div>
                    <Table striped bordered hover variant="primary">
                     <thead>
                       <tr >
                         <th>Product Name</th>
                         <th>Price</th>
                         <th>Quantity</th>
                         <th>CheckOut</th>
                       </tr>
                     </thead>
                     <tbody>
                       { productCart }
                     </tbody>
                   </Table>
        </>

    );
}