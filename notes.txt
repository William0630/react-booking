Creating React application:
	Syntax: npx create-react-app <project-name>

Application > src
Delete the following file:
	- App.test.js
	- index.css
	- logo.svg
	- reportWebVitals.js

Remove the importation of the deleted files.
Application > src > index.js
	- index.css
	- reportWebVitals.js (including reportWebVitals())

Application > src > App.js
	- logo.svg
	- elements under <div> tag

Install the JavaScript (Babel) Linting for code readability.
	- Open Command Palette
			Linux and Windows
				Ctrl + Shift + P
			MacOS
				Cmd + Shift + P
	- 	In the input field type the word "install" and select the "Package Control: Install Package" option to trigger an installation of an add-on feature for sublime text editor.

	- 	Type "babel" in the input field to search for the "Babel" linting to be installed.

	- Change the linting of the sublime text editor to "Javascript(Babel)"


ReactJS components
	- This are reusable parts of our react application.

	- They are independent UI parts of our app. 

  	- Components are functions that return react elements.

  	- Components naming convention: PascalCase

  	- Components should start in capital letters.

Install this package for the React Routing:
	- npm install react-router-dom
		-  react-router-dom - allows us to simulate page switching for our Reactjs app.
		- Reactjs was first conceptualized to be used on SPA (single page application). So instead, we will simulate within our Reactjs the switching of pages.

useContext Mini Activity:
Using React JS Context, redirect the user to the "/courses route" (Course page) whenever the "/register route" (Register page) is accessed and a user is logged in.

Send a screenshot of your browser upon finishing the activity.

